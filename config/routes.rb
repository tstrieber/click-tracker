Rails.application.routes.draw do
  resources :pages, only: :index
  resources :clicks, only: [:index, :create]
  root 'pages#index'
end
