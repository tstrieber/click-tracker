require 'rails_helper'

RSpec.describe Click, type: :model do
  it 'has a valid factory' do
    expect(build(:click)).to be_valid
  end

  describe 'validation' do
    it 'is invalid without x' do
      expect(build(:click, x: nil)).to be_invalid
    end

    it 'is invalid without y' do
      expect(build(:click, y: nil)).to be_invalid
    end

    it 'is invalid without width' do
      expect(build(:click, width: nil)).to be_invalid
    end

    it 'is invalid without height' do
      expect(build(:click, height: nil)).to be_invalid
    end

    it 'is invalid without scroll_x' do
      expect(build(:click, scroll_x: nil)).to be_invalid
    end

    it 'is invalid without scroll_y' do
      expect(build(:click, scroll_y: nil)).to be_invalid
    end
  end
end
