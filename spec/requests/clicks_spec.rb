require 'rails_helper'

RSpec.describe Click, type: :request do
  describe 'POST #create' do
    let!(:click_attributes) { attributes_for(:click) }

    before(:each) do
      post clicks_path, click: click_attributes
    end

    it 'creates a click' do
      expect(Click.count).to eq 1
    end
  end

  describe 'GET #index' do
    let!(:click1) { create(:click, x: 0, y: 0) }
    let!(:click2) { create(:click, x: 1, y: 1) }
    let!(:click3) { create(:click, x: 1, y: 1) }

    before(:each) do
      get clicks_path

      @heatmap = JSON.parse(response.body)
    end

    it 'returns a heatmap' do
      expect(@heatmap).to be
    end

    it 'has a min' do
      expect(@heatmap['min']).to eq 1
    end

    it 'has a max' do
      expect(@heatmap['max']).to eq 2
    end

    it 'has data' do
      expect(@heatmap['data'].count).to eq 2
    end
  end
end
