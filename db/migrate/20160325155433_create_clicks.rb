class CreateClicks < ActiveRecord::Migration
  def change
    create_table :clicks do |t|
      t.integer :x
      t.integer :y
      t.integer :width
      t.integer :height
      t.integer :scroll_x
      t.integer :scroll_y

      t.timestamps null: false
    end
  end
end
