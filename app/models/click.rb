# == Schema Information
#
# Table name: clicks
#
#  id         :integer          not null, primary key
#  x          :integer
#  y          :integer
#  width      :integer
#  height     :integer
#  scroll_x   :integer
#  scroll_y   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Click < ActiveRecord::Base

  validates :x, :y, :width, :height, :scroll_x, :scroll_y,
    numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  scope :to_heatmap, -> do
    results = select(:x, :y, 'count(id) as value').group(:x, :y).order('value DESC').map { |c| c.slice(:x, :y, :value) }

    { max: results.first[:value], min: results.last[:value], data: results }
  end
end
