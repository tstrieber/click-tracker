class ClicksController < ApplicationController
  def index
    render json: Click.to_heatmap
  end

  def create
    click = Click.new(click_params)

    if click.save
      render json: click
    else
      render json: click.errors, status: :unprocessable_entity
    end
  end

  private

  def click_params
    params.require(:click).permit(:x, :y, :width, :height, :scroll_x, :scroll_y)
  end
end
